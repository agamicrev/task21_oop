package com.vertsimaha;

import com.vertsimaha.menu.Menu;

public final class Main {

  private Main() {
  }

  public static void main(final String[] args) {
    Menu menu = new Menu();
    menu.menu();
  }
}
